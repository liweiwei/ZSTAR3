#include "irq.h"

/* IRQ ISR */
void irq_isr() __interrupt(2) {
    if (IRQ_IRQSC->IRQF) {
        IRQ_IRQSC->IRQACK = IRQ_SET_BIT;
    }
}

void irq_init() {
    // 使能IRQ脚, IRQ中断
    IRQ_IRQSC->IRQPE = IRQ_SET_BIT;
    IRQ_IRQSC->IRQIE = IRQ_SET_BIT;
}

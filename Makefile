# 编译信息
VERBOSE ?= 0

BUILD = build

ifeq ($(VERBOSE), 0)
Q = @
endif

RM = rm
ECHO = @echo
MKDIR = mkdir

CC = sdcc
SIZE = size

# 编译文件
SRC = watchdog.c
SRC += irq.c
SRC += spi.c
SRC += gpio.c

OBJ = $(addprefix $(BUILD)/, $(SRC:.c=.rel))

DEP_FILES = main.c Makefile

CFLAGS = -ms08 --opt-code-size --fomit-frame-pointer --Werror

LFLAGS = --data-loc 0x0080 --stack-loc 0x107F --code-loc 0x182C

ifneq ($(VERBOSE), 0)
CFLAGS += --verbose
endif

CFLAGS += $(LFLAGS)

.PHONY: all clean

all: $(BUILD)/firmware.s19
	$(ECHO) "*****Firmware Compiled Successfully*****"

$(BUILD)/firmware.s19: $(OBJ) $(DEP_FILES)
	$(Q)$(CC) $(CFLAGS) main.c $(OBJ) -o $@
	$(Q)$(SIZE) $@

$(BUILD)/%.rel: %.c %.h
	$(Q)$(CC) $(CFLAGS) -c $< -o $@

$(OBJ): | $(BUILD)

$(BUILD):
	$(Q)$(MKDIR) -p $@

clean:
	$(Q)$(RM) -rf $(BUILD)

#ifndef __IRQ_H__
#define __IRQ_H__

typedef struct {
    volatile char IRQMOD: 1;
    volatile char IRQIE: 1;
    volatile char IRQACK: 1;
    volatile char IRQF: 1;
    volatile char IRQPE: 1;
    volatile char IRQEDG: 1;
    volatile char BIT6: 1;
    volatile char BIT7: 1;
} IRQ_IRQSC_Type;

#define IRQ_IRQSC_BASE (0x0014)

#define IRQ_IRQSC ((IRQ_IRQSC_Type *) IRQ_IRQSC_BASE)

#define IRQ_SET_BIT (0x01)
#define IRQ_CLEAR_BIT (0x00)

void irq_init();

#endif // __IRQ_H__

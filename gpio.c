#include "gpio.h"

/* KBI ISR */
void gpio_kbd7_isr() __interrupt(22) {
    // 翻转D1
    if (GPIO_KBI->KBISC.KBF) {
        GPIO_C->PTxD.BIT5 = !GPIO_C->PTxD.BIT5;

        GPIO_KBI->KBISC.KBACK = GPIO_SET_BIT;
    }
}

void gpio_init() {
    GPIO_C->PTxDD.BIT5 = GPIO_SET_BIT;

    GPIO_A->PTxPE.BIT7 = GPIO_SET_BIT; // 必须下拉, 以防干扰
    GPIO_KBI->KBIPE.KBIPE7 = GPIO_SET_BIT;
    GPIO_KBI->KBISC.KBIE = GPIO_SET_BIT;

    // 设置连接Modem的GPIO
    GPIO_D->PTxDD.BIT0 = GPIO_SET_BIT;
    GPIO_D->PTxDD.BIT1 = GPIO_SET_BIT;

    GPIO_D->PTxD.BIT3 = GPIO_SET_BIT; // 低电复位, 所以先设置为High
    GPIO_D->PTxDD.BIT3 = GPIO_SET_BIT;

    GPIO_E->PTxD.BIT2 = GPIO_SET_BIT; // SPI SS
    GPIO_E->PTxPE.BIT2 = GPIO_SET_BIT;
    GPIO_E->PTxDD.BIT2 = GPIO_SET_BIT;

    GPIO_E->PTxPE.BIT6 = GPIO_SET_BIT; // 必须上拉,
    GPIO_E->PTxPE.BIT7 = GPIO_SET_BIT; // 因为Modem的GPIO 1/2没有上拉
}

#ifndef __GPIO_H__
#define __GPIO_H__

typedef struct {
    volatile char BIT0: 1;
    volatile char BIT1: 1;
    volatile char BIT2: 1;
    volatile char BIT3: 1;
    volatile char BIT4: 1;
    volatile char BIT5: 1;
    volatile char BIT6: 1;
    volatile char BIT7: 1;
} GPIO_Byte;

typedef struct {
    GPIO_Byte PTxD;
    GPIO_Byte PTxPE;
    GPIO_Byte PTxSE;
    GPIO_Byte PTxDD;
} GPIO_Type;

typedef struct {
    volatile char KBIMOD: 1;
    volatile char KBIE: 1;
    volatile char KBACK: 1;
    volatile char KBF: 1;
    volatile char KBEDG4: 1;
    volatile char KBEDG5: 1;
    volatile char KBEDG6: 1;
    volatile char KBEDG7: 1;
} GPIO_KBISC_Type;

typedef struct {
    volatile char KBIPE0: 1;
    volatile char KBIPE1: 1;
    volatile char KBIPE2: 1;
    volatile char KBIPE3: 1;
    volatile char KBIPE4: 1;
    volatile char KBIPE5: 1;
    volatile char KBIPE6: 1;
    volatile char KBIPE7: 1;
} GPIO_KBIPE_Type;

typedef struct {
    GPIO_KBISC_Type KBISC;
    GPIO_KBIPE_Type KBIPE;
} GPIO_KBI_Type;

#define GPIO_A_BASE (0x0000)
#define GPIO_B_BASE (0x0004)
#define GPIO_C_BASE (0x0008)
#define GPIO_D_BASE (0x000C)
#define GPIO_E_BASE (0x0010)

#define GPIO_KBI_BASE (0x0016)

#define GPIO_A ((GPIO_Type *) GPIO_A_BASE)
#define GPIO_B ((GPIO_Type *) GPIO_B_BASE)
#define GPIO_C ((GPIO_Type *) GPIO_C_BASE)
#define GPIO_D ((GPIO_Type *) GPIO_D_BASE)
#define GPIO_E ((GPIO_Type *) GPIO_E_BASE)

#define GPIO_KBI ((GPIO_KBI_Type *) GPIO_KBI_BASE)

#define GPIO_SET_BIT (0x01)
#define GPIO_CLEAR_BIT (0x00)

void gpio_init();

#endif // __GPIO_H__

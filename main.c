#include "watchdog.h"
#include "irq.h"
#include "spi.h"
#include "gpio.h"

/* IRQ ISR */
void irq_isr() __interrupt(2);

/* SPI ISR */
void spi_isr() __interrupt(15);

/* KBI ISR */
void gpio_kbd7_isr() __interrupt(22);

void core_init() {
    watchdog_init();
    irq_init();
    spi_init();
    gpio_init();
}

void main() {
    // 初始化内核
    core_init();

    // 使能中断
    __asm__ ("CLI");

    // 永不退出
    for (;;) {}
}

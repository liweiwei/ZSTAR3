# 简介
[**ZSTAR3**](https://gitlab.com/LiWeiwei/ZSTAR3)是[**Freescale ZSTAR3**](https://www.nxp.com/products/no-longer-manufactured/zstar3:RD3172MMA7455L)开发板的Demo程序.

# 编译链
*ZSTAR3*使用[*SDCC*](http://sdcc.sourceforge.net)编译器, 并使用[*GNU Make*](http://www.gnu.org/software/make)管理编译流程.

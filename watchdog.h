#ifndef __WATCHDOG_H__
#define __WATCHDOG_H__

typedef struct {
    volatile char BIT0: 1;
    volatile char BKGDPE: 1;
    volatile char BIT2: 1;
    volatile char BIT3: 1;
    volatile char STOPT: 1;
    volatile char STOPE: 1;
    volatile char COPT: 1;
    volatile char COPE: 1;
} WATCHDOG_SOPT_Byte;

#define WATCHDOG_SOPT_BASE (0x1802)

#define WATCHDOG_SOPT ((WATCHDOG_SOPT_Byte *) WATCHDOG_SOPT_BASE)

#define WATCHDOG_SET_BIT (0x01)
#define WATCHDOG_CLEAR_BIT (0x00)

void watchdog_init();

#endif // __WATCHDOG_H__

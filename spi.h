#ifndef __SPI_H__
#define __SPI_H__

#include "gpio.h"

typedef struct {
    volatile char LSBFE: 1;
    volatile char SSOE: 1;
    volatile char CPHA: 1;
    volatile char CPOL: 1;
    volatile char MSTR: 1;
    volatile char SPTIE: 1;
    volatile char SPE: 1;
    volatile char SPIE: 1;
} SPI_SPIC1_Type;

typedef struct {
    volatile char SPC0;
    volatile char SPISWAI;
    volatile char BIT2;
    volatile char BIDIROE;
    volatile char MODFEN;
    volatile char BIT5;
    volatile char BIT6;
    volatile char BIT7;
} SPI_SPIC2_Type;

typedef struct {
    volatile char SPR0;
    volatile char SPR1;
    volatile char SPR2;
    volatile char BIT3;
    volatile char SPPR0;
    volatile char SPPR1;
    volatile char SPPR2;
    volatile char BIT7;
} SPI_SPIBR_Type;

typedef struct {
    volatile char BIT0;
    volatile char BIT1;
    volatile char BIT2;
    volatile char BIT3;
    volatile char MODF;
    volatile char SPTEF;
    volatile char BIT6;
    volatile char SPRF;
} SPI_SPIS_Type;

typedef struct {
    SPI_SPIC1_Type SPIC1;
    SPI_SPIC2_Type SPIC2;
    SPI_SPIBR_Type SPIBR;
    SPI_SPIS_Type SPIS;
    volatile char BYTE4;
    volatile char SPID;
} SPI_Type;

#define SPI_BASE (0x0028)

#define SPI ((SPI_Type *) SPI_BASE)

#define SPI_SET_BIT (0x01)
#define SPI_CLEAR_BIT (0x00)

void spi_ss_on();
void spi_ss_off();

void spi_init();

#endif // __SPI_H__

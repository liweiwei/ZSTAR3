#include "spi.h"

/* SPI ISR */
void spi_isr() __interrupt(15) {
    // 读SPI数据
    if (SPI->SPIS.SPRF) {
        SPI->SPID;
    }

    // 写SPI数据
    if (SPI->SPIS.SPTEF) {

        if (1) { // 数据发送完毕, 禁止发中断
            SPI->SPIC1.SPTIE = SPI_CLEAR_BIT;
        }
    }
}

void spi_ss_on() {
    GPIO_E->PTxD.BIT2 = GPIO_CLEAR_BIT;
}

void spi_ss_off() {
    GPIO_E->PTxD.BIT2 = GPIO_SET_BIT;
}

void spi_init() {
    // MCU SPI为主
    SPI->SPIC1.MSTR = SPI_SET_BIT;

    // 开启接收中断
    SPI->SPIC1.SPIE = SPI_SET_BIT;

    // MCU处于Wait模式时, 关闭SPI CLK
    SPI->SPIC2.SPISWAI = SPI_SET_BIT;

    // 使能SPI
    SPI->SPIC1.SPE = SPI_SET_BIT;
}
